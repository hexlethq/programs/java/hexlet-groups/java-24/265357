package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String end;

        end = cardNumber.substring(cardNumber.length() - 4, cardNumber.length());

        String start = "";
        for (int i = 0; i < starsCount; i++) {
            start = start + "*";
        }
        System.out.println(start + end);
        // END
    }
}
