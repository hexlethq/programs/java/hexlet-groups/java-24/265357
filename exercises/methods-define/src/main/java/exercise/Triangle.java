package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(double lenA, double lenB, double degrees) {
        return lenA * lenB * (double)0.5 * (double)Math.PI * degrees / (double)180;
    }

    public static void main(String[] args) {
        var area = getSquare(4, 5, 45);
        System.out.printf("Area of the triangle of 4 by 5 with angle 45 degrees = %lf\n", area);
    }
    // END
}
