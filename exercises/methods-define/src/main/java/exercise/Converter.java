package exercise;

class Converter {
    public static int convert(int sizeOfFile, String direction) {
        if (direction.equals("b")) {
            return sizeOfFile * 1024;
        } else if (direction.equals("kb")) {
            return sizeOfFile / 1024;
        } else {
            return 0;
        }

    }

    public static void main(String[] args) {
        System.out.println("10 kb = " + convert(10, "b"));
    }
    
    // END
}
