package exercise;

import org.junit.jupiter.api.Test;

import static com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut;
import static org.assertj.core.api.Assertions.assertThat;

class AppTest {
    @Test
    void testNumbers() throws Exception {
        System.out.println("testNumbers..");
        String result = tapSystemOut(exercise.App::numbers);
        assertThat(result.trim()).isEqualTo("5");
    }

    @Test
    void testStrings() throws Exception {
        System.out.println("testStrings..");
        String result = tapSystemOut(exercise.App::strings);
        assertThat(result.trim()).isEqualTo("Java works on JVM");
    }

    @Test
    void testConverting() throws Exception {
        System.out.println("testConverting..");
        String result = tapSystemOut(exercise.App::converting);
        assertThat(result.trim()).isEqualTo("300 spartans");
    }

    public static void main(String [] args) throws Exception {
        AppTest appTest = new AppTest();
        appTest.testNumbers();
        appTest.testStrings();
        appTest.testConverting();
    }
}
