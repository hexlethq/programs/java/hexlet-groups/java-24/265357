package exercise;

class App {
    // BEGIN
    public static int main(String[] args) {
        String acro1 = App.getAbbreviation("Portable Network Graphics");
        String acro2 = App.getAbbreviation("Complementary metal oxide semiconductor");
        System.out.println("acro1 = " + acro1 + ", acro2 = " + acro2);
        return 0;
    }

    public static String getAbbreviation(String fullString) {

        String acronym = "";
        String[] words = fullString.split(" ");
        for (String word : words) {
            acronym += word.substring(0,1).toUpperCase();
        }
        return acronym;
    }
    // END
}
